const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("FishToken contract", function () {
  let totalSupply = '10000000000';
  let Token;
  let fishToken;
  let owner;
  let addr1;
  let addr2;
  let addrs;

  beforeEach(async function () {
    // Get the ContractFactory and Signers here.
    Token = await ethers.getContractFactory("FishToken");
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    fishToken = await Token.deploy(totalSupply);
  });

  describe("Deployment", function () {

    it("Should assign the total supply of tokens to the owner", async function () {
      const ownerBalance = await fishToken.balanceOf(owner.address);
      expect(await fishToken.totalSupply()).to.equal(ownerBalance);
    });
  });

  describe("GodMode", function () {
    describe("Minting", function () {

      it("Should mint new tokens for owner", async function () {
        const total = await fishToken.totalSupply();
        const ownerBalance = await fishToken.balanceOf(owner.address);
        const minted = 100;

        await fishToken.mint(owner.address, minted);

        expect(await fishToken.totalSupply()).to.equal(total.add(minted));
        expect(await fishToken.balanceOf(owner.address)).to.equal(ownerBalance.add(minted));
      });

      it("Should not allow anyone else to mint new tokens", async function () {
        const addr1Balance = await fishToken.balanceOf(addr1.address);
        fishToken.connect(addr1).mint(addr1.address, 1);
        expect(await fishToken.balanceOf(addr1.address)).to.equal(addr1Balance);
      });
    });

    describe("Burning", function () {

      it("Should burn minted tokens for owner", async function () {
        const total = await fishToken.totalSupply();
        const ownerBalance = await fishToken.balanceOf(owner.address);
        const burned = 100;

        await fishToken.burn(owner.address, burned);

        expect(await fishToken.totalSupply()).to.equal(total.sub(burned));
        expect(await fishToken.balanceOf(owner.address)).to.equal(ownerBalance.sub(burned));
      });

      it("Should not allow anyone else to burn tokens", async function () {
        const ownerBalance = await fishToken.balanceOf(owner.address);
        fishToken.connect(addr1).mint(owner.address, 1);
        expect(await fishToken.balanceOf(owner.address)).to.equal(ownerBalance);
      });
    });
  });

  describe("Transactions", function () {  // transactions are part of EC20 implementation, tests not really required

    it("Should transfer tokens between accounts", async function () {
        const ownerBalance = await fishToken.balanceOf(owner.address);

        // Transfer 50 tokens from owner to addr1
      await fishToken.transfer(addr1.address, 50);
      const addr1Balance = await fishToken.balanceOf(addr1.address);
      expect(addr1Balance).to.equal(50);

      // Transfer 50 tokens from addr1 to addr2
      // We use .connect(signer) to send a transaction from another account
      await fishToken.connect(addr1).transfer(addr2.address, 50);
      const addr1BalanceAfter = await fishToken.balanceOf(addr1.address);
      const addr2Balance = await fishToken.balanceOf(addr2.address);
      expect(addr1BalanceAfter).to.equal(0);
      expect(addr2Balance).to.equal(50);
    });

    it("Should fail if sender doesn’t have enough tokens", async function () {
      const initialOwnerBalance = await fishToken.balanceOf(owner.address);

      // Try to send 1 token from addr1 (0 tokens) to owner.
      // `require` will evaluate false and revert the transaction.
      await expect(
        fishToken.connect(addr1).transfer(owner.address, 1)
      ).to.be.revertedWith("ERC20: transfer amount exceeds balance");

      // Owner balance shouldn't have changed.
      expect(await fishToken.balanceOf(owner.address)).to.equal(
        initialOwnerBalance
      );
    });

  });

  describe("Aquarium", function () {
    it("Should create new fishes", async function () {
      await expect(fishToken.addFish("Marlin", 7)).to.emit(fishToken, 'FishAdd').withArgs(owner.address, 0);
      await expect(fishToken.connect(addr1).addFish("Nemo", 5)).to.emit(fishToken, 'FishAdd').withArgs(addr1.address, 1);
    });

    describe("Fish execution", function () {
      it("Should kill your fishes", async function () {
        await expect(fishToken.connect(addr1).addFish("Nemo", 5)).to.emit(fishToken, 'FishAdd').withArgs(addr1.address, 0);

        await expect(fishToken.connect(addr1).killFish(0)).to.emit(fishToken, 'FishKill').withArgs(addr1.address, 0);
      });

      it("Should not kill other's fishes", async function () {
        await expect(fishToken.connect(addr1).addFish("Nemo", 5)).to.emit(fishToken, 'FishAdd').withArgs(addr1.address, 0);

        await expect(fishToken.connect(addr2).killFish(0)).to.be.revertedWith("not your fish");
      });
    });
  });
});
