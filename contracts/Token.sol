pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";  // onlyOwner
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";  // ERC20

struct Fish {
    address owner;
    uint id;
    string name;
    uint size;
}

contract FishToken is Ownable, ERC20 {
    uint nextId;
    mapping (uint => Fish) public aquarium;

    event FishAdd(address owner, uint id);
    event FishKill(address owner, uint id);


    constructor(uint256 initialSupply) ERC20("FishToken", "FSH") {
        _mint(msg.sender, initialSupply);
        nextId = 0;
    }

    function mint(address account, uint256 amount) public onlyOwner {
        _mint(account, amount);
    }

    function burn(address account, uint256 amount) public onlyOwner {
        _burn(account, amount);
    }

    function addFish(string memory name, uint size) public {
        uint id = nextId;
        nextId += 1;
        aquarium[id] = Fish(msg.sender, id, name, size);

        emit FishAdd(msg.sender, id);
    }

    function killFish(uint id) public {
        require(aquarium[id].owner == msg.sender, "not your fish");
        delete aquarium[id];

        emit FishKill(msg.sender, id);
    }
}
